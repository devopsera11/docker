# Use the alpine Linux base image
FROM alpine:latest

# Install Bash, curl, and jq using apk package manager
RUN apk update && apk add --no-cache bash curl jq

# Set the working directory
WORKDIR /usr/src/app

# Copy the Bash scripts into the container
COPY script.sh entrypoint.sh ./

# Make the Bash scripts executable
RUN chmod +x ./script.sh ./entrypoint.sh

# Set the entrypoint to the custom shell script
ENTRYPOINT ["./entrypoint.sh"]
