#!/bin/bash

# Function to retrieve the token from the external data source API
get_token() {
  local token_url="https://151dd0e4-bd8b-453b-a01c-924e75053a8b.mock.pstmn.io/auth"
  local token_response=$(curl -s "$token_url")
  echo "$token_response"
}

# Function to retrieve PARAMETER1 and PARAMETER2 using the provided token
get_parameters() {
  local parameters_url="https://151dd0e4-bd8b-453b-a01c-924e75053a8b.mock.pstmn.io/parameters"
  local token="$1"
  local parameters_response=$(curl -s -G "$parameters_url" --data-urlencode "TOKEN=$token")
  echo "$parameters_response"
}

# Main script
main() {
  # Retrieve the token
  local token=$(get_token)

  # Check if token is not empty
  if [[ -z "$token" ]]; then
    echo "Failed to retrieve the token. Exiting..."
    exit 1
  fi

  # Retrieve PARAMETER1 and PARAMETER2 using the token
  local parameters=$(get_parameters "$token")

  # Check if parameters are not empty
  if [[ -z "$parameters" ]]; then
    echo "Failed to retrieve parameters. Exiting..."
    exit 1
  fi

  # Extract PARAMETER1 and PARAMETER2 from the parameters JSON response
  local parameter1=$(echo "$parameters" | jq -r '.PARAMETER1')
  local parameter2=$(echo "$parameters" | jq -r '.PARAMETER2')

  # Generate the Kubernetes deployment YAML for the "Hello World" app
  cat <<EOF >deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-world-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-world
  template:
    metadata:
      labels:
        app: hello-world
    spec:
      containers:
      - name: hello-world
        image: alpine:latest
        command: ["/bin/sh", "-c"]
        args: ["echo HELLO WORLD , $parameter1 - $parameter2 && sleep infinity"]
EOF



  echo "Deployment YAML generated and saved to deployment.yaml."
  echo "after deploying the deployment.yaml file you should see the below output: HELLO WORLD , $parameter1 - $parameter2"
}

main
